<?php
    if(isset($_GET['pos'])) {
        $pos = $_GET['pos'];
        $txts = array("animal", "name", "place", "verb", "adverb", "adjective");
        $flag = 0;

        //テキストファイルオープン
        for ($i = 0; $i < count($txts); $i++){
            if (strcmp($pos, $txts[$i]) == 0){
                $file = file_get_contents('./getword/'.$pos.'.txt');
                $flag = 1;
            }
        }
        if($flag != 1) {//animal, decade, kaomoji以外であればランダムにファイルを開く
            $n = rand(0, count($txts) - 1);
            $file = file_get_contents('./getword/'.$txts[$n].'.txt');
        }

        $file = explode("\n", $file);//ファイルを1行毎に配列に格納
        $file_len = count($file);//ファイルの行数を取得

        $n = rand(0, $file_len - 1);//乱数生成

        //nozomi-step-hw7

        print($file[$n]);
    }

?>