<?php
    $txts = array("animal", "name", "place", "verb", "adverb", "adjective");
    $out_text = "";
    $getword = "";

    //STEPerのURLを取得
    $urls = file_get_contents('http://step-test-krispop.appspot.com/peers?endpoint=getword', false, $context);
	$urls = explode("\n", $urls);
    $urls_len = count($urls);



    //テキストファイルから構文を取得
    $strs = file_get_contents('./madlib.txt');
	$strs = explode("\n", $strs);
    $strs_len = count($strs);
    $rand_strs = rand(0, $strs_len - 1);

    echo $strs[$rand_strs]."<br>";

    //単語取得
    for ($i = 0; $i < strlen($strs[$rand_strs]); $i++){
        
        //$strs[$rand_strs]に[name]などのgetwords部分があればランダムなサーバからgetwordする
    	if ($strs[$rand_strs][$i] == '['){
    		$i++;
    		while($strs[$rand_strs][$i] != ']'){
    			$getword .= $strs[$rand_strs][$i];
    			$i++;
    		}
    		$i++;

    		$rand_urls = rand(0, $urls_len - 1);

            $out_text .= file_get_contents($urls[$rand_urls].'./getword?pos='.$getword, false, $context);
             
        }
        $getword = "";

    	$out_text .= $strs[$rand_strs][$i];

    }

    echo $out_text;

?>
